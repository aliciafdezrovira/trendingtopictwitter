package org.aliciafdezrov.TrendingTopicTwitter;

import java.util.Arrays;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class WordCount {

	public static void main(String[] args){
		if(args.length !=1){
			System.out.println("error expected one argument");
			throw new RuntimeException();
		}
		
		//Step 1: Create a SparkConf object
		SparkConf conf = new SparkConf().setAppName("Spark demo");
				
		//Step 2: Create a JavaSpark context
		JavaSparkContext sparkContext = new JavaSparkContext(conf);
		
		//Step 3: Creamos el JavaRDD
		JavaRDD<String> lines = sparkContext.textFile(args[0]);
		
		//Step 4: Metemos las palabras de cada linea en un array de palabras
		JavaRDD<Strin> words = lines.flatMap(s) -> {return Arrays.asList(s.split(" "));}; Forma simplicada del paso 4
		
		//Step 5: Para cada palabra crear un par clave-valor
		JavaPairRDD<String, Integer> pairs = words.mapToPair(new PairFunction<String, String, Integer>(){
			public Tuple2<String, Integer> call(String s) throws Exception{
				return new Tuple2<String, Integer>(s, 1);
			}
		});	
		
		//Step 7: Agrupar todos los pares con la misma clave
		JavaPairRDD<String, Integer> counts = pairs.reduceByKey(new Function2<Integer, Integer, Integer>(){
			public Integer call(Integer integer, Integer integer2) throws Exception{
				return integer + integer2;
			}
		});
		
		//Step 8: Ordenar los pares
		
		JavaPairRDD<String, Integer> orderedPairs = counts.sortByKey();
		
		List<Tuple2<String, Integer>> output = orderedPairs.collect();
	
		for(Tuple2<?, ?> tuple: output){
			System.out.println(tuple._1() + ": " + tuple._2());
		}
	}
	
}
